[![Twitter Badge](https://img.shields.io/badge/-@aguirresabino-1ca0f1?style=flat-square&labelColor=1ca0f1&logo=twitter&logoColor=white&link=https://twitter.com/aguirresabino)](https://twitter.com/aguirresabino) [![Linkedin Badge](https://img.shields.io/badge/-aguirresabino-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/aguirresabino/)](https://www.linkedin.com/in/aguirresabino/)

## Hi 👋, 
I'm Aguirre Sabino, I'm a software engineer 👨‍💻 who is passionate about open source.

- 🔭 I’m currently working on Adireto
- 🌱 I’m currently learning full cycle development model

> Skinny Models, Skinny Controllers, Fat Services

## Programming language, frameworks and tools

### Know/Using

[<img src="https://i.imgur.com/PlV6Sua.png" alt="HTML5" width="24">]()
[<img src="https://i.imgur.com/GID3ieB.png" alt="CSS" width="24">]()
[<img src="https://i.imgur.com/WNuhhQb.png" alt="JS" width="24">]()
[<img src="https://i.imgur.com/xaEzNyf.png" alt="Typescript" width="24">](https://www.typescriptlang.org/)
[<img src="https://i.imgur.com/jS1bn1e.png" alt="Angular" width="24">](https://angular.io/)
[<img src="https://i.imgur.com/40npomR.jpg" alt="Materialize CSS" width="24">](https://materializecss.com/)
[<img src="https://i.imgur.com/8e42Ugi.png" alt="Bootstrap" width="24">](https://getbootstrap.com/) 
[<img src="https://i.imgur.com/SMtbhxU.png" alt="Bash" width="24">]() 
[<img src="https://i.imgur.com/BxcFUlR.png" alt="Docker" width="24">](https://hub.docker.com/u/aguirresabino) 
[<img src="https://i.imgur.com/0IFYzrq.png" alt="Express" width="24">](https://expressjs.com/) 
[<img src="https://i.imgur.com/J6yfdyQ.png" alt="Git" width="24">](https://git-scm.com/)
[<img src="https://i.imgur.com/2GzTX7r.png" alt="GitHub" width="24">](https://github.com/aguirresabino)
[<img src="https://i.imgur.com/9gMn8DY.png" alt="GitLab" width="24">](https://gitlab.com/aguirresabino)
[<img src="https://i.imgur.com/FZdzXca.png" alt="GraphQL" width="24">](https://graphql.org/)
[<img src="https://i.imgur.com/ivdERuh.png" alt="Intellij IDEA" width="24">](https://www.jetbrains.com/idea/)
[<img src="https://i.imgur.com/7kdJBwP.png" alt="Java" width="24">](https://www.java.com/)
[<img src="https://i.imgur.com/TdyXb4e.png" alt="Jest" width="24">](https://jestjs.io/)
[<img src="https://i.imgur.com/H1ULAiZ.png" alt="Linux" width="24">]()
[<img src="https://i.imgur.com/JsppkaK.png" alt="MongoDB" width="24">](https://www.mongodb.com/)
[<img src="https://i.imgur.com/jSXcDVP.png" alt="NestJS" width="24">](https://nestjs.com/)
[<img src="https://i.imgur.com/g2gamql.png" alt="NodeJS" width="24">](https://nodejs.org/)
[<img src="https://i.imgur.com/5BjiB59.png" alt="NPM" width="24">](https://www.npmjs.com/~aguirresabino)
[<img src="https://i.imgur.com/a0Ui05W.png" alt="Oh My Zsh" width="24">](https://ohmyz.sh/)
[<img src="https://i.imgur.com/lDTyjLG.png" alt="PostgresSQL" width="24">](https://www.postgresql.org/)
[<img src="https://i.imgur.com/K6RsiKi.png" alt="Redis" width="24">](https://redis.io/)
[<img src="https://i.imgur.com/crF7nXv.png" alt="Spring" width="24">](https://spring.io/)
[<img src="https://i.imgur.com/3Q43tkP.png" alt="Visual Studio Code" width="24">](https://code.visualstudio.com/)
[<img src="https://i.imgur.com/Msn69QK.png" alt="Webstorm" width="24">](https://www.jetbrains.com/webstorm/)
[<img src="https://i.imgur.com/dQOij9i.png" alt="Yarn" width="24">](https://yarnpkg.com/)
[<img src="https://i.imgur.com/YCj9DaZ.png" alt="RabbitMQ" width="24">](https://www.rabbitmq.com/)
[<img src="https://i.imgur.com/QzjZWpd.png" alt="Heroku" width="24">](https://www.heroku.com/)

## Learning

[<img src="https://i.imgur.com/pFVIuQ9.png" alt="Go" width="24">](https://golang.org/)
[<img src="https://i.imgur.com/9Ft20Mq.png" alt="Kubernets" width="24">](https://kubernetes.io/)
[<img src="https://i.imgur.com/FHSYgif.png" alt="PHP" width="24">](https://www.php.net/)
[<img src="https://i.imgur.com/RpOq7fq.png" alt="ReactJS" width="24">](https://reactjs.org/)
[<img src="https://i.imgur.com/O6yeX8t.png" alt="Prometheus" width="24">](https://prometheus.io/)
[<img src="https://i.imgur.com/IWouopb.png" alt="Elastic Stack" width="24">](https://www.elastic.co/elastic-stack)

